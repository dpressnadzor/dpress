import { useState } from 'react';
import { ComponentMeta } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Header } from '../src/renderer/components/Header';
import { AddPostPageContent } from '../src/renderer/components/AddPostPageContent';
import { IAccount } from '../src/interfaces';

interface IArgs {
  accounts: IAccount[];
}

export default {
  title: 'Pages/AddPost',
  component: AddPostPageContent,
} as ComponentMeta<typeof AddPostPageContent>;


export const Story = ({
  accounts,
}: IArgs) => {
  const onAccountSelected = action('onAccountSelected');
  const [selectedAccount, setSelectedAccount] = useState(accounts[0]);
  return (
    <div>
      <Header
        accounts={accounts}
        selectedAccount={selectedAccount}
        onAccountSelected={(account) => {
          onAccountSelected(account);
          setSelectedAccount(account);
        }}
      />
      <AddPostPageContent
      />
    </div>
  );
}
const args: IArgs = {
  accounts: [
    {displayName: '7thias', publicKeyDigest: 'MD5:14:5a:86:3c:56:14:97:0e:44:ff:db:bc:df:c7:c1:70'},
    {displayName: 'account2', publicKeyDigest: 'MD5:22:33:44:55:66:77:88:99:12:13:14:15:16:17:18:19'},
  ],
};
Story.args = args;
