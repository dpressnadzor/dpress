import { useState } from 'react';
import { ComponentMeta } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Header } from '../src/renderer/components/Header';
import { PostsPageContent } from '../src/renderer/components/PostsPageContent';
import { IAccount, IPostMeta } from '../src/interfaces';

interface IArgs {
  accounts: IAccount[];
  posts: IPostMeta[];
}

export default {
  title: 'Pages/Posts',
  component: PostsPageContent,
} as ComponentMeta<typeof PostsPageContent>;


export const Story = ({
  accounts,
  posts,
}: IArgs) => {
  const onAccountSelected = action('onAccountSelected');
  const [selectedAccount, setSelectedAccount] = useState(accounts[0]);
  return (
    <div>
      <Header
        accounts={accounts}
        selectedAccount={selectedAccount}
        onAccountSelected={(account) => {
          onAccountSelected(account);
          setSelectedAccount(account);
        }}
      />
      <PostsPageContent
        currentPagePosts={posts}
      />
    </div>
  );
}
const args: IArgs = {
  accounts: [
    {displayName: '7thias', publicKeyDigest: 'MD5:14:5a:86:3c:56:14:97:0e:44:ff:db:bc:df:c7:c1:70'},
    {displayName: 'account2', publicKeyDigest: 'MD5:22:33:44:55:66:77:88:99:12:13:14:15:16:17:18:19'},
  ],
  posts: [
    {
      title: 'My First Post',
      datePosted: new Date('2022-01-30T14:59:03Z').getTime(),
      tags: ['free speech', 'environment'],
    },
    {
      title: 'My Second Post',
      datePosted: new Date('2022-01-31T14:59:03Z').getTime()
      ,
      tags: ['colonialism', 'europe'],
    },
    {
      title: 'My Other Post',
      datePosted: new Date('2022-02-04T14:59:03Z').getTime(),
      tags: ['taxes', 'military'],
    },
    {
      title: 'My Second Other Post',
      datePosted: new Date('2022-05-15T14:59:03Z').getTime(),
      tags: ['education', 'environment'],
    },
    {
      title: 'Coral reefs',
      datePosted: new Date('2022-05-15T14:59:03Z').getTime(),
      tags: ['environment'],
    },
    {
      title: 'Oil Companies',
      datePosted: new Date('2022-05-15T14:59:03Z').getTime(),
      tags: ['petroleum', 'environment'],
    },
    {
      title: 'Desalinization',
      datePosted: new Date('2022-05-15T14:59:03Z').getTime(),
      tags: ['water'],
    },
  ],
};
Story.args = args;
