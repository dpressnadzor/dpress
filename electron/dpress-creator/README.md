# dpress-creator

dPress Content Creation part. Contains administration part of dPress and IPFS node for pinning


## Development

Run `yarn storybook` to develop UI components in storybook.

Run `yarn start` to develop the electron app.
