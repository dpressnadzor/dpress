
export interface IElectronAPI {
  test: () => void;
}


export interface IAccount {
  displayName: string;
  publicKeyDigest: string;
}

export interface IPostMeta {
  title: string;
  /** timestamp, ms */
  datePosted: number;
  tags?: string[];
}


export interface IOrbitDbInited {
  address: string;
  identity: string;
  own: boolean;
  instance: unknown;
}

export interface IArticleDTO {
  _id: string;
  thumbnail: string;
  createdAt: number;
  title: string;
  html: string;
}
