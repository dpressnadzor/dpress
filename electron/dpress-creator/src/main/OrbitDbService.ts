import type { IPFS } from 'ipfs';
import OrbitDb, { IOrbitDbInstance, IOrbitStore } from 'orbit-db';
import { APP_CONFIG } from '../environment';


export interface IOrbitDbOpts {
  ipfsInstance: IPFS;
}

export class OrbitDbService {
  pOrbitInstance: Promise<IOrbitDbInstance>;
  pOrbitStore: Promise<IOrbitStore>;

  constructor ({
    ipfsInstance,
  }: IOrbitDbOpts) {
    this.pOrbitInstance = OrbitDb.createInstance(ipfsInstance);

    this.pOrbitStore = this.pOrbitInstance.then((orbitInstance) => {
      const orbitStore = orbitInstance.docstore(
        APP_CONFIG.rootAddress,
        {
          create: true,
          sync: false,
        }
      );
      console.log({orbitStore});
      return orbitStore;
    });
  }

  test () {
    console.log('Hello from OrbitDbService in the main thread');
  }
}
