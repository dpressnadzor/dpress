import { create, IPFS, Options as IpfsOptions } from 'ipfs-core';
import type { IDResult, VersionResult } from 'ipfs-core-types/src/root';
import {
  BehaviorSubject,
  filter,
  first,
  from,
  map,
  Observable,
  switchMap,
  tap,
} from 'rxjs';


export interface IIpfsServiceOpts {
  ipfsConfig: IIpfsConfig;
}
export type IIpfsConfig = IpfsOptions;

export class IpfsService {
  private _ipfsSource = new BehaviorSubject<null | IPFS>(null);
  public ipfs$ = this._ipfsSource
    .asObservable()
    .pipe(filter((x) => !!x)) as Observable<IPFS>
  ;
  private pNode: Promise<IPFS>;

  constructor ({
    ipfsConfig,
  }: IIpfsServiceOpts) {
    console.log('Starting new node...');
    this.pNode = create(ipfsConfig);

    this.pNode.then(async (node) => {
      console.log('Node created...', node);

      await this.connectToPeers();
      setInterval(async () => await this.connectToPeers(), 15000)
      this._ipfsSource.next(node);
    });
  }

  /**
   * Get the ID information about the current IPFS node
   */
  getIPFSData(): Observable<IDResult> {
    return this.ipfs$.pipe(switchMap((ipfs) => from(ipfs!.id())));
  }

  /**
   * Get the version information about the current IPFS node
   */
  getVersion(): Observable<VersionResult> {
    return this.ipfs$.pipe(switchMap((ipfs) => from(ipfs!.version())));
  }

  /**
   * Get the status of the current IPFS node
   */
  getStatus(): Observable<boolean> {
    return this.ipfs$.pipe(map((ipfs) => ipfs.isOnline()));
  }

  uploadFile(
    fileContent:
      | Uint8Array
      | Blob
      | Iterable<Uint8Array>
      | AsyncIterable<Uint8Array>
      | ReadableStream<Uint8Array>
      | ArrayBuffer
  ): Observable<unknown> {
    return this.ipfs$.pipe(
      switchMap(async (ipfs) => {
        console.log('fileContent', fileContent);
        try {
          const result = await ipfs.add(fileContent);
          console.log(result);
          return result;
        } catch (e) {
          console.error(e);
        }
      }),
      tap((file) => console.log('file saved', file))
    );
  }

  getFile(path: string) {
    return this.ipfs$.pipe(
      switchMap((ipfs) => from(ipfs.cat(path))),
      first()
    );
  }

  private async connectToPeers() {
    const node = await this.pNode;
    const peers = await node.swarm.peers();
    console.log('peers', peers);
    await Promise.all(peers.map(async (peer) => {
      if (peers.indexOf(peer) !== -1) return
      try {
        await node.ping(peer)
        await node.swarm.connect('/p2p-circuit/ipfs/' + peer)
      } catch (e) {
        console.error(e)
      }
    }));
  }
}
