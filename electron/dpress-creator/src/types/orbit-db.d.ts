/* eslint-disable @typescript-eslint/no-empty-interface */

declare module 'orbit-db' {
  export function createInstance (ipfs: IPFS): Promise<IOrbitDbInstance>;

  export interface IOrbitDbInstance {
    docstore: (
      p0: unknown,
      p1: unknown,
    ) => IOrbitStore;
  }

  export interface IOrbitStore {

  }
}
