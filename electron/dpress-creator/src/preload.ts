import { contextBridge, ipcRenderer } from 'electron';
import type { IElectronAPI } from './interfaces';

const api: IElectronAPI = {
  test: () => {
    ipcRenderer.postMessage('test', undefined);
  },
  // openFile: () => ipcRenderer.invoke('dialog:openFile'),
};

contextBridge.exposeInMainWorld('electronAPI', api);

declare global {
  interface Window {
    electronAPI: IElectronAPI;
  }
}
