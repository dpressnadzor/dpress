import { Box, Container, Typography } from '@mui/material';

import type { IPostMeta } from '../../interfaces';
import { PostsList } from './PostsList';
import { SearchInput } from './SearchInput';


export const PostsPageContent = ({
  currentPagePosts,
}: {
  currentPagePosts: IPostMeta[];
}) => {
  return (
    <Container style={{
      paddingTop: '2rem',
    }}>
      <div style={{
        marginBottom: '1.6rem',
      }}>
        <div style={{
          alignItems: 'center',
          display: 'flex',
        }}>
          <Typography variant="h1" component="span" sx={{
            fontSize: '2rem',
            fontWeight: 100,
            flexGrow: 1,
          }}>
            Posts
          </Typography>
          <SearchInput style={{width: 320}} placeholder="Search" />
        </div>
      </div>
      <Box sx={{ flexGrow: 1 }}>
        {currentPagePosts.length === 0 ? (
          <Typography>No posts</Typography>
        ) : (
          <PostsList posts={currentPagePosts} />
        )}
      </Box>
    </Container>
  );
};
