import { Typography } from '@mui/material';
import { formatRelative, formatRFC7231 } from 'date-fns'


import type { IPostMeta } from '../../interfaces';


export const PostsList = ({
  posts,
}: {
  posts: IPostMeta[];
}) => {
  return (
    <table style={{
      borderSpacing: '1rem 2rem',
      width: '100%',
    }}>
      <thead style={{
        textAlign: 'left',
      }}>
        <tr>
          <th>Title</th>
          <th>Date posted</th>
          <th>Tags</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      {posts.map((post, i) => {
        return (
          <tr key={i}>
            <td>
              <Typography>{post.title}</Typography>
            </td>
            <td>
              <Typography>
                <span title={formatRFC7231(post.datePosted)}>
                  {formatRelative(post.datePosted, Date.now())}
                </span>
              </Typography>
            </td>
            <td>
              <Typography>{post.tags?.join(', ')}</Typography>
            </td>
            <td>
              <Typography>
                share
                {' | '}
                edit
                {' | '}
                delete
              </Typography>
            </td>
          </tr>
        );
      })}
      </tbody>
    </table>
  );
}
