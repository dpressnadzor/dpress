import { InputUnstyled, inputUnstyledClasses, InputUnstyledProps } from '@mui/base';
import { styled } from '@mui/system';
import { Search as SearchIcon } from '@mui/icons-material';

import { forwardRef } from 'react';


const StyledInputRoot = styled('div')(
  () => `
  font-family: IBM Plex Sans, sans-serif;
  display: inline-flex;
  font-weight: 400;
  border: 1px solid #ccc;
  border-radius: 3em;
  background: #f3f3f3;
  align-items: center;
  justify-content: center;

  &.${inputUnstyledClasses.focused} {
    outline: 3px solid #daecff;
  }

  &:hover {
    background: #eee;
    border-color: #bbb;
  }
`,
);

const StyledInputElement = styled('input')(
  () => `
  font-size: 0.875rem;
  font-family: inherit;
  font-weight: 400;
  line-height: 1.5;
  flex-grow: 1;
  color: #111;
  background: inherit;
  border: none;
  border-radius: inherit;
  padding: 12px 12px;
  padding-left: 1.5em;
  outline: 0;
`,
);

const CustomInput = forwardRef(function CustomInput(
  props: InputUnstyledProps,
  ref: React.ForwardedRef<HTMLDivElement>,
) {
  const { components, ...other } = props;
  return (
    <InputUnstyled
      components={{
        Root: StyledInputRoot,
        Input: StyledInputElement,
        ...components,
      }}
      {...other}
      ref={ref}
    />
  );
});

const InputAdornment = styled('div')(() => /* css */`
  margin: 8px;
  margin-right: 1em;
  display: inline-flex;
  align-items: center;
  justify-content: center;
`);

export const SearchInput = forwardRef(function SearchInput (
  props: InputUnstyledProps,
  ref: React.ForwardedRef<HTMLDivElement>,
) {
  return (
    <CustomInput
      {...props}
      ref={ref}
      endAdornment={
        <InputAdornment>
          <SearchIcon />
        </InputAdornment>
      }
    />
  );
});
