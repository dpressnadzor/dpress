/**
 * The posts page is where the post content is authored, previewed, and published.
 *
 * Posts are authored as markdown.
 *
 * To support images and other media, these can be loaded into memory, and be
 * previewed using `URL.createObjectURL()`.
 * When publishing, these files need to be published alongside the post (TBD how?)
 *
 * Previewing could be immediate --- inline or side-by-side, or a separate tab of the editor page.
 *
 * Use `rich-markdown-editor`.
 *
 * To figure out:
 * - What are the publishing parameters? Pinning services?
 *
 * Resolved:
 * - Routing --- reuse the Add page as the Edit page? Nah -- there's no editing yet.
 * - Preview with themes? No --- content will be published without themes, but as standard markdown.
 *
 *
 */
import { Container, Typography } from '@mui/material';


export const AddPostPageContent = () => {
  return (
    <Container style={{
      paddingTop: '2rem',
    }}>
      <div style={{
        marginBottom: '1.6rem',
      }}>
        <div style={{
          alignItems: 'center',
          display: 'flex',
        }}>
          <Typography variant="h1" component="span" sx={{
            fontSize: '2rem',
            fontWeight: 100,
            flexGrow: 1,
          }}>
            Add Post
          </Typography>
        </div>
      </div>
      ...
    </Container>
  );
};
