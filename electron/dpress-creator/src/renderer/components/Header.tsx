import {
  AppBar,
  Box,
  Button,
  Toolbar,
  Typography,
  IconButton,
  Menu,
  MenuItem,
} from '@mui/material';
import {
  ArrowDropDown,
  Menu as MenuIcon,
} from '@mui/icons-material';

import type { IAccount } from '../../interfaces';
import { useState } from 'react';


/**
 * App Header
 */
export const Header = ({
  accounts,
  selectedAccount,
  onAccountSelected,
}: {
  accounts: IAccount[];
  selectedAccount: IAccount;
  onAccountSelected: (account: IAccount) => void;
}) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{
        backgroundColor: '#404040',
      }}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Box sx={{ flexGrow: 1 }}>
            <LogoAndVersion />
          </Box>
          <AccountSwitcher
            accounts={accounts}
            selectedAccount={selectedAccount}
            onAccountSelected={onAccountSelected}
          />
        </Toolbar>
      </AppBar>
    </Box>
  );
};


export const LogoAndVersion = () => {
  return (
    <Box>
      <Typography variant="h6" component="span" sx={{
        marginRight: '0.5rem',
        fontStyle: 'italic',
        fontWeight: 900,
      }}>
        dPress
      </Typography>
      <Typography variant="caption" component="span" style={{
        opacity: 0.7,
      }}>
        v1.0.0
      </Typography>
    </Box>
  );
};

export const AccountSwitcher = ({
  accounts,
  selectedAccount,
  onAccountSelected,
}: {
  accounts: IAccount[];
  selectedAccount: IAccount;
  onAccountSelected: (account: IAccount) => void;
}) => {
  const hasMore = selectedAccount && accounts.length > 1;
  const contents = (
    !selectedAccount ? (
      <Typography variant="body1">
        No accounts
      </Typography>
    ) : (
      <>
        <div style={{
          width: '2rem',
          height: '2rem',
          background: 'rgba(255, 255, 255, 0.2)',
          borderRadius: '9rem',
          marginRight: '0.8rem',
        }} />
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
        }}>
          <div>
            <Typography variant="body1"
              style={{
                lineHeight: 1,
                fontWeight: 'bold',
                fontSize: '0.8rem',
                marginBottom: '0.2rem',
              }}
            >
              {selectedAccount.displayName}
            </Typography>
          </div>
          <Typography variant="caption"
            style={{
              lineHeight: 1,
              fontSize: '0.6rem',
              userSelect: 'text',
            }}
          >
            {selectedAccount.publicKeyDigest}
          </Typography>
        </div>
        {hasMore ? (
          <ArrowDropDown  style={{marginLeft: '0.5rem'}}/>
        ) : undefined}
      </>
    )
  );

  // Menu opening
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = !!anchorEl;
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  if (hasMore) {
    return (
      <>
        <Button
          sx={{
            background: 'rgba(255, 255, 255, 0.2)',
            padding: '0.5rem',
            paddingRight: '1rem',
            borderRadius: '9rem',
            display: 'flex',
            alignItems: 'center',
            color: 'white',
            textTransform: 'none',
            textAlign: 'inherit',
            cursor: 'pointer',
            userSelect: 'none',
            '&:hover': {
              background: 'rgba(255, 255, 255, 0.3)',
            },
          }}
          onClick={handleClick}
        >
          {contents}
        </Button>
        <Menu
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          {accounts.map((account) => (
            <MenuItem
              key={account}
              onClick={() => {
                handleClose();
                if (account !== selectedAccount) {
                  onAccountSelected(account);
                }
              }}
              style={{
                backgroundColor: (account === selectedAccount) ? '#eee' : 'transparent',
              }}
            >
              <div style={{lineHeight: 0}}>
                <div>
                  <Typography variant="body1"
                    style={{
                      lineHeight: 1,
                      fontWeight: 'bold',
                      fontSize: '0.8rem',
                      marginBottom: '0.2rem',
                    }}
                  >
                    {account.displayName}
                  </Typography>
                </div>
                <Typography variant="caption"
                  style={{
                    lineHeight: 1,
                    fontSize: '0.6rem',
                    userSelect: 'text',
                  }}
                >
                  {account.publicKeyDigest}
                </Typography>
              </div>
            </MenuItem>
          ))}
        </Menu>
      </>
    );
  } else {
    return (
      <Box sx={{
        background: 'rgba(255, 255, 255, 0.2)',
        padding: '0.5rem',
        paddingRight: '1rem',
        borderRadius: '9rem',
        display: 'flex',
        alignItems: 'center',
        color: 'white',
      }}>
        {contents}
      </Box>
    );
  }
};
