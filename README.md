# dPress

Monorepo for dPress creator, web UI, and others.

# Getting Started

This monorepo uses Rush. 
Please read [Getting started as a developer](https://rushjs.io/pages/developer/new_developer/)
and [Everyday commands](https://rushjs.io/pages/developer/everyday_commands/).

Install deps:
```sh
rush update
```

We configure rush to use pnpm as the package manager. Common commands:
- Add: `rush add --caret -p pkg` or `rush add --exact --dev -p pkg`
- Audit: `pnpm audit -C common/temp`
- See outdated: `pnpm outdated -rC common/temp`
- See version: `pnpm why -rC common/temp pkg`
- List: `rush list`

Configure your git username and email locally for this repo:
```
git config --local user.name "MyName"
git config --local user.email "https://gitlab.com/myusername"
```

[Install an Editorconfig plugin](https://editorconfig.org/#pre-installed) for your IDE, if necessary.
Install the [VSCode Eslint plugin](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) to show eslint errors in VSCode.




# dPress Creator

Run the dPress Creator electron app:
```sh
cd ./electron/dpress-creator && rushx start
```

Run the storybook:
```sh
cd ./electron/dpress-creator && rushx storybook
```


# Todo
- Add license to repo.
